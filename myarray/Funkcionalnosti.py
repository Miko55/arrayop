import basearray as ba
import numpy as np

def izpis(arr):
    if len(arr.shape)==2:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1])
        for v in arr:
                for s in v:
                    print("{}".format(s).rjust(5), end="")
                print(end="\n")
    elif len(arr.shape)==1:
        arr = np.asarray(arr._BaseArray__data)
        for n in arr:
            print("{}".format(n).rjust(5), end="")
        print(end="\n")
    elif len(arr.shape)==3:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1],arr.shape[2])
        for v in arr:
            for s in v:
                print("[",end="")
                for g in s:
                    print("{}".format(g).rjust(5), end="")
                print("]",end="")
            print(end="\n")
    else:
        return False

def merge_lists(left_sublist,right_sublist):
	i,j = 0,0
	result = []
	while i<len(left_sublist) and j<len(right_sublist):
		if left_sublist[i] <= right_sublist[j]:
			result.append(left_sublist[i])
			i += 1
		else:
			result.append(right_sublist[j])
			j += 1
	result += left_sublist[i:]
	result += right_sublist[j:]
	return result

def merge_sort(input_list):
	if len(input_list) <= 1:
		return input_list
	else:
		midpoint = int(len(input_list)/2)
		left_sublist = merge_sort(input_list[:midpoint])
		right_sublist = merge_sort(input_list[midpoint:])
		return merge_lists(left_sublist,right_sublist)


def sortiraj(arr,nacin):
    if len(arr.shape)>1:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1])
        if nacin=='vertikalno':
            st=0
            for n in arr:
                arr[:,st]=merge_sort(arr[:,st].tolist())
                st+=1
            l=np.reshape(arr, arr.shape[0]*arr.shape[1])
            rarr=ba.BaseArray((arr.shape[0],arr.shape[1]), data=l.tolist())
            return rarr
        elif nacin=='horizontalno':
            st=0
            for n in arr:
                arr[st]=merge_sort(arr[st].tolist())
                st+=1
            l=np.reshape(arr, arr.shape[0]*arr.shape[1])
            rarr=ba.BaseArray((arr.shape[0],arr.shape[1]), data=l.tolist())
            return rarr
        else:
            return False
    elif len(arr.shape)==1:
        arr = np.asarray(arr._BaseArray__data)
        if nacin=='horizontalno':
            arr=merge_sort(arr.tolist())
            rarr=ba.BaseArray((len(arr),), data=arr)
            return rarr
        else:
            return False


def najdi(arr,x):
    i=0
    j=0
    najdeno=[]
    if len(arr.shape)==2:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1])
        for v in arr:
            j=0
            for s in v:
                if s==x:
                    najdeno.append(str(i)+","+str(j))
                j+=1
            i+=1
        return najdeno
    elif len(arr.shape)==1:
        arr = np.asarray(arr._BaseArray__data)
        i=0
        for n in arr:
            if n==x:
                najdeno.append(str(i))
            i+=1
        return najdeno
    elif len(arr.shape)==3:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1],arr.shape[2])
        for v in arr:
            j=0
            for s in v:
                z=0
                for g in s:
                    if g==x:
                        najdeno.append(str(i)+","+str(j)+","+str(z))
                    z+=1
                j+=1
            i+=1
        return najdeno
    else:
        return False

def operacijaSkalar(arr, skalar ,operacija):
    if len(arr.shape)==1:
        arr = np.asarray(arr._BaseArray__data)
    elif len(arr.shape)==2:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1])
    elif len(arr.shape)==3:
        arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1],arr.shape[2])
    else:
        return False
    if operacija=='sestej':
        arr = arr+skalar
    elif operacija=='odstej':
        arr = arr-skalar
    elif operacija=='zmnozi':
        arr = arr*skalar
    elif operacija=='deli':
        arr = arr/skalar
    elif operacija=='potenciraj':
        i=0
        j=0
        for v in arr:
            j=0
            for s in v:
                potenca=arr[i,j]
                for n in range(skalar-1):
                    potenca=potenca*arr[i,j]
                arr[i,j]=potenca
                j+=1
            i+=1
    else:
        return False
    if len(arr.shape)==1:
        rarr=ba.BaseArray((len(arr),), data=arr)
    elif len(arr.shape)==2:
        l=np.reshape(arr, arr.shape[0]*arr.shape[1])
        rarr=ba.BaseArray((arr.shape[0],arr.shape[1]), data=l.tolist())
    elif len(arr.shape)==3:
        l=np.reshape(arr, arr.shape[0]*arr.shape[1]*arr.shape[2])
        rarr=ba.BaseArray((arr.shape[0],arr.shape[1],arr.shape[2]), data=l.tolist())
    return rarr


def mnoziMatriki(arr,arr2):
    if len(arr.shape)>1 and len(arr2.shape)>1:
        if arr.shape[1]==arr2.shape[0]:
            arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1])
            arr2 = np.asarray(arr2._BaseArray__data).reshape(arr2.shape[0],arr2.shape[1])
            zmnozek=np.matmul(arr,arr2)
            l=np.reshape(zmnozek, zmnozek.shape[0]*zmnozek.shape[1])
            rarr=ba.BaseArray((zmnozek.shape[0],zmnozek.shape[1]), data=l.tolist())
            return rarr
        else:
            return False
    else:
        return False

def sestejMatriki(arr,arr2):
    if arr.shape == arr2.shape:
        if len(arr.shape)==1:
            arr = np.asarray(arr._BaseArray__data)
            arr2 = np.asarray(arr2._BaseArray__data)
            sestevek=arr+arr2
            rarr=ba.BaseArray((len(sestevek),), data=sestevek)
        elif len(arr.shape)==2:
            arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1])
            arr2 = np.asarray(arr2._BaseArray__data).reshape(arr2.shape[0],arr2.shape[1])
            sestevek=arr+arr2
            l=np.reshape(sestevek, sestevek.shape[0]*sestevek.shape[1])
            rarr=ba.BaseArray((sestevek.shape[0],sestevek.shape[1]), data=l.tolist())
        elif len(arr.shape)==3:
            arr = np.asarray(arr._BaseArray__data).reshape(arr.shape[0],arr.shape[1],arr.shape[2])
            arr2 = np.asarray(arr2._BaseArray__data).reshape(arr2.shape[0],arr2.shape[1],arr.shape[2])
            sestevek=arr+arr2
            l=np.reshape(sestevek, sestevek.shape[0]*sestevek.shape[1]*sestevek.shape[2])
            rarr=ba.BaseArray((sestevek.shape[0],sestevek.shape[1],sestevek.shape[2]), data=l.tolist())
        return rarr
    else:
        return False