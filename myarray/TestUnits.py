import unittest
import basearray as ba
import Funkcionalnosti as fs
import numpy
import sys
import io

class Test_TestUnits(unittest.TestCase):
    def test_izpis1D(self):
        rezultat = io.StringIO()
        sys.stdout = rezultat
        a = ba.BaseArray((5,), data=(12, 5, 8, 7, 14))
        fs.izpis(a)
        sys.stdout = sys.__stdout__
        resitev="   12    5    8    7   14\n"
        self.assertEqual(resitev,rezultat.getvalue())

    def test_izpis2D(self):
        rezultat = io.StringIO()
        sys.stdout = rezultat
        a = ba.BaseArray((3,3), data=(12, 5, 8, 7, 14, 20, 21, 2, 13))
        fs.izpis(a)
        sys.stdout = sys.__stdout__
        resitev="   12    5    8\n    7   14   20\n   21    2   13\n"
        self.assertEqual(resitev,rezultat.getvalue())

    def test_izpis3D(self):
        rezultat = io.StringIO()
        sys.stdout = rezultat
        a = ba.BaseArray((3,1,2), data=tuple(range(6)))
        fs.izpis(a)
        sys.stdout = sys.__stdout__
        resitev="[    0    1]\n[    2    3]\n[    4    5]\n"
        self.assertEqual(resitev,rezultat.getvalue())

    def test_izpisNegativnihStevil(self):
        rezultat = io.StringIO()
        sys.stdout = rezultat
        a = ba.BaseArray((3,3), data=(12, -5, -8, 7, -14, 20, 21, -2, 13))
        fs.izpis(a)
        sys.stdout = sys.__stdout__
        resitev="   12   -5   -8\n    7  -14   20\n   21   -2   13\n"
        self.assertEqual(resitev,rezultat.getvalue())

    def test_izpisDecimalnihStevil(self):
        rezultat = io.StringIO()
        sys.stdout = rezultat
        a = ba.BaseArray((3,3), data=(1.2, 5.5, 8.75, 7.99, 14., 2.0, 21.1, 2.1, 1.3))
        fs.izpis(a)
        sys.stdout = sys.__stdout__
        resitev="  1.2  5.5 8.75\n 7.99 14.0  2.0\n 21.1  2.1  1.3\n"
        self.assertEqual(resitev,rezultat.getvalue())

    def test_izpisVelikaStevila(self):
        rezultat = io.StringIO()
        sys.stdout = rezultat
        a = ba.BaseArray((3,3), data=(12, -512, 85231, 7.3211, 1.4, 0, 159328132, -0.33333, 123))
        fs.izpis(a)
        sys.stdout = sys.__stdout__
        resitev="           12        -512    85231\n       7.3211         1.4        0\n    159328132    -0.33333      123\n"
        self.assertEqual(resitev,rezultat.getvalue())

    def test_soritiranjeHorizontalno(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 7, 14, 20, 21, 2, 13))
        rezultat = fs.sortiraj(a,'horizontalno')
        resitev = numpy.array([[ 5,8,12], [ 7,14,20], [ 2,13,21]])
        resitev = ba.BaseArray((3,3), data=(5,8,12,7,14,20,2,13,21))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_soritiranjeVertikalno(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 7, 14, 20, 21, 2, 13))
        rezultat = fs.sortiraj(a,'vertikalno')
        resitev = ba.BaseArray((3,3), data=(7,2,8,12,5,13,21,14,20))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_sortiranje1D(self):
        a = ba.BaseArray((5,), data=(12, 5, 18, 3, 2))
        rezultat = fs.sortiraj(a,'horizontalno')
        resitev = ba.BaseArray((5,), data=(2,3,5,12,18))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_sortiranjeNapacno(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 7, 14, 20, 21, 2, 13))
        rezultat = fs.sortiraj(a,'diagonalno')
        self.assertFalse(rezultat)
    
    def test_iskanjeVeckrat(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.najdi(a,5)
        resitev= ['0,1','1,0','2,1']
        self.assertEqual(resitev,rezultat)

    def test_iskanje1D(self):
        a = ba.BaseArray((5,), data=(12, 5, 8, 5, 14))
        rezultat = fs.najdi(a,5)
        resitev= ['1','3']
        self.assertEqual(resitev,rezultat)

    def test_iskanje3D(self):
        a = ba.BaseArray((3,3,2), data=tuple(range(18)))
        rezultat = fs.najdi(a,6)
        resitev=['1,0,0']
        self.assertEqual(resitev,rezultat)

    def test_iskanjeNobenkrat(self):
        a = ba.BaseArray((5,), data=(12, 7, 8, 9, 14))
        rezultat = fs.najdi(a,5)
        resitev= []
        self.assertEqual(resitev,rezultat)

    def test_skalarSestavanje(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.operacijaSkalar(a,5,'sestej')
        resitev = ba.BaseArray((3,3), data=(17,10,13,10,19,25,26,10,18))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_skalarOdstavanje(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.operacijaSkalar(a,5,'odstej')
        resitev = ba.BaseArray((3,3), data=(7,0,3,0,9,15,16,0,8))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)
    
    def test_skalarMnozenje(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.operacijaSkalar(a,5,'zmnozi')
        resitev = ba.BaseArray((3,3), data=(60,25,40,25,70,100,105,25,65))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_skalarDeljenje(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.operacijaSkalar(a,5,'deli')
        resitev = ba.BaseArray((3,3), data=(2.4,1.,1.6,1.,2.8,4.,4.2,1.,2.6))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_skalarPotenciranje(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.operacijaSkalar(a,5,'potenciraj')
        resitev = ba.BaseArray((3,3), data=(248832, 3125, 32768,3125, 537824, 3200000,4084101,3125,371293))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_skalar3D(self):
        a = ba.BaseArray((2,3,2), data=tuple(range(12)))
        rezultat = fs.operacijaSkalar(a,5,'sestej')
        resitev = ba.BaseArray((2,3,2), data=(5,6,7,8,9,10,11,12,13,14,15,16))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_skalarNapacno(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        rezultat = fs.operacijaSkalar(a,5,'obrni')
        self.assertFalse(rezultat)

    def test_sestejMatriki(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        b = ba.BaseArray((3,3), data=(1, 2, 3, 4, 5, 6, 7, 8, 9))
        rezultat = fs.sestejMatriki(a,b)
        resitev = ba.BaseArray((3,3), data=(13,7,11,9,19,26,28,13,22))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_sestej3D(self):
        a = ba.BaseArray((3,1,2), data=tuple(range(6)))
        b = ba.BaseArray((3,1,2), data=tuple(range(6)))
        rezultat = fs.sestejMatriki(a,b)
        resitev = [[[0,2]],[[4,6]],[[8,10]]]
        resitev = ba.BaseArray((3,1,2), data=(0,2,4,6,8,10))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_sestejMatrikiNapacno(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        b = ba.BaseArray((2,2), data=(1, 2, 3, 4))
        rezultat = fs.sestejMatriki(a,b)
        self.assertFalse(rezultat)

    def test_mnoziMatriki(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        b = ba.BaseArray((3,3), data=(1, 2, 3, 4, 5, 6, 7, 8, 9))
        rezultat = fs.mnoziMatriki(a,b)
        resitev = ba.BaseArray((3,3), data=(88,113,138,201,240,279,132,171,210))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)

    def test_mnoziMatrikiNapacno(self):
        a = ba.BaseArray((3,3), data=(12, 5, 8, 5, 14, 20, 21, 5, 13))
        b = ba.BaseArray((2,3), data=(1, 2, 3, 4, 5, 6))
        rezultat = fs.mnoziMatriki(a,b)
        self.assertFalse(rezultat)

    def test_rezultatFloat(self):
        a = ba.BaseArray((3,3), data=(5, 5, 5, 5, 10, 10, 10, 15, 15))
        rezultat = fs.operacijaSkalar(a,0.8,'deli')
        resitev = ba.BaseArray((3,3), data=(6.25,6.25,6.25,6.25,12.5,12.5,12.5,18.75,18.75))
        self.assertEqual(resitev.__dict__,rezultat.__dict__)
       

if __name__ == '__main__':
    unittest.main(exit=False)
